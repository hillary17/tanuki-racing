// Link to test answer on LeetCode: https://leetcode.com/problems/sum-of-left-leaves/?envType=featured-list

// Sum of Left Leaves Problem : We are given the root of a binary tree, return the sum of all left leaves in our tree. Provide two different solutions for us to choose from.